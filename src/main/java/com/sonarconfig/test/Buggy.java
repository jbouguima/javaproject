package com.sonarconfig.test;

public class Buggy  implements Cloneable {

	private Integer magicNumber;

	public Buggy(Integer magicNumber) {
		this.magicNumber = magicNumber;
	}

	public boolean isBuggy(String x) {
		return "Buggy".equals(x);
	}

	public boolean equals(Object o) {
		if (o instanceof Buggy) {
			return ((Buggy) o).magicNumber == magicNumber;
		}
		if (o instanceof Integer) {
			return magicNumber == ((Integer) 0);
		}
		return false;
	}

	Buggy() {

	}

	static class MoreBuggy extends Buggy {
		static MoreBuggy singleton = new MoreBuggy();
	}

	static MoreBuggy foo = MoreBuggy.singleton;
}